const express = require("express")
const body_Parser = require("body-parser")

const app = express()
const api = require('./routes/rutas')

app.use(function(req, res, next) {
res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
res.header('Access-Control-Allow-Credentials', true);
next();
});

app.use(body_Parser.urlencoded({ extended: false }))
app.use(body_Parser.json())
app.use('/telco', api)

module.exports = app