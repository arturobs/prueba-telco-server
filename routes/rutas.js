'use strict'

const express = require('express')
const prueba_Ctrl = require('../controllers/prueba')
const usuarios_Ctrl = require('../controllers/usuarios')
const vehiculosCtrl = require('../controllers/vehiculos')
const auth = require('../middlewares/auth')
const api = express.Router()


/*rutas de prueba*/
api.get('/prueba', prueba_Ctrl.prueba)
api.get('/private', auth.is_Auth, prueba_Ctrl.prueba_Privada)

/*rutas de usuarios*/
api.post('/nuevo_Usuario', usuarios_Ctrl.nuevo_Usuario)
api.post('/iniciar_sesion', usuarios_Ctrl.logIn)

/*rutas de vehiculos*/
api.post('/nuevo_Vehiculo', auth.is_Auth, vehiculosCtrl.agregar_Vehiculo)
api.get('/get_Vehiculos', auth.is_Auth, vehiculosCtrl.get_Vehiculos)
api.put('/update_Vehiculos', auth.is_Auth, vehiculosCtrl.update_Vehiculos)
api.delete('/delete_Vehiculos', auth.is_Auth, vehiculosCtrl.delete_Vehiculos)


module.exports = api