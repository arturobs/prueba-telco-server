'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const config = require('../config')

function create_Token(user) {
    
    const payload = {
        sub: user.Id,
        tipo: user.Tipo,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}

module.exports = {
    create_Token
}