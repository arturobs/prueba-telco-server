'use strict'

const mysql = require('mysql2')

const port = process.env.PORT || 3001;
const SECRET_TOKEN = 'cXVlIGNoaW5nYWRvcyBhbmRhcyBidXNjYW5kbyBwZXJybz8=';
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'telco',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  });

  module.exports = {
      port,
      pool,
      SECRET_TOKEN
  }
