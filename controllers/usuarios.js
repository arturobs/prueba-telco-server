'use strict'

const config = require('../config')
const crypto = require('crypto')
const service = require('../services')

function nuevo_Usuario(req,res){

    var User = req.body.usuario;
    var Pass = req.body.password;
    var Tipo = req.body.tipo;// 1-Admin(ve todos los vehiculos), 2-usuario(solo ve sus vehiculos)

    config.pool.getConnection(function(err, conn) {

          conn.execute(
              `SELECT * FROM usuarios WHERE Usuario = ?`,
              [User],
              function(err, results, fields){
                  if(!err){                    
                      if(results.length > 0){
                        res.status(400).send({ status: false, message: `El usuario ${User} ya esta dado de alta`});
                      }else{

                        var Salt = crypto.randomBytes(16).toString('hex');
                        var Hash = crypto.pbkdf2Sync(Pass, Salt, 1000, 64, `sha512`).toString('hex');

                        conn.execute(
                            'INSERT INTO usuarios (Usuario, Password, Salt, Tipo) VALUES (?,?,?,?)',
                            [User,Hash,Salt,Tipo],
                            function(err, results, fields) {
                                if(!err){
                                    return res.status(200).send({ status: true, message: `El usuario ${User} fue registrado exitosamente`});
                                }else{
                                    console.log(err);
                                }               
                            }
                          );                        
                      }                      
                  }else{
                      console.log(err);
                  }
              }
          );
        
        config.pool.releaseConnection(conn);
     })
}

function logIn(req,res){

    var User = req.body.usuario;
    var Pass = req.body.password; 

    config.pool.getConnection(function(err, conn) {

          conn.execute(
              `SELECT * FROM usuarios WHERE Usuario = ?`,
              [User],
              function(err, results, fields){
                  if(!err){
                    if(results.length > 0){

                        var salt = results[0].Salt; 
                        var Hash = crypto.pbkdf2Sync(Pass, salt, 1000, 64, `sha512`).toString('hex');
                        if(Hash === results[0].Password){
                           return res.status(201).send({ status: true , token: service.create_Token(results[0])});
                        }else{
                           res.status(400).send({ status: false, message: `El password es incorrecto`});
                        }

                    }else{
                        res.status(400).send({ status: false, message: `El usuario no esta registrado`});
                    }                             
                  }else{
                      console.log(err);
                  }
              }
          );
        
        config.pool.releaseConnection(conn);
     })
}

module.exports = {
    nuevo_Usuario,
    logIn
}