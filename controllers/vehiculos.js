'use strict'

const config = require('../config')

function agregar_Vehiculo(req,res){

    
    var placas = req.body.placas;
    var marca = req.body.marca;
    var color = req.body.color;
    var modelo = req.body.modelo;
    var posicion = req.body.posicion;
    var usuario_id = req.body.usuario_id;

    config.pool.getConnection(function(err, conn) {

        conn.execute(
            'INSERT INTO vehiculos (Placas, Marca, Color, Modelo, Posicion, usuario_id) VALUES (?,?,?,?,?,?)',
            [placas,marca,color,modelo,posicion,usuario_id],
            function(err, results, fields) {
                if(!err){
                    return res.status(200).send({ message: `El vehiculo con placa: ${placas} fue registrado exitosamente`});
                }else{
                    console.log(err);
                }               
            }
          ); 


        config.pool.releaseConnection(conn);
     })

}

function get_Vehiculos(req,res){

    var usuario_id = req.query.ui;
    var usuario_tipo = req.query.ut;

    config.pool.getConnection(function(err, conn) {

        if(usuario_tipo == 1){
            conn.execute(
                'SELECT * FROM vehiculos',
                [],
                function(err, results, fields) {
                    if(!err){
                        return res.status(200).send({ results });
                    }else{
                        console.log(err);
                    }               
                }
              );
        }else{
            conn.execute(
                'SELECT * FROM vehiculos WHERE usuario_id = ?',
                [usuario_id],
                function(err, results, fields) {
                    if(!err){
                        return res.status(200).send({ results });
                    }else{
                        console.log(err);
                    }               
                }
              );
        }

        config.pool.releaseConnection(conn);
     })

    

}

function update_Vehiculos(req,res){    

    var placas = req.body.placas;
    var marca = req.body.marca;
    var color = req.body.color;
    var modelo = req.body.modelo;
    var posicion = req.body.posicion;
    var cuenta_comas = 0;

    var query = 'UPDATE vehiculos SET';

    if(marca){
        query+= ` Marca = '${marca}'`;
        cuenta_comas++;
    }

    if(color){
        if(cuenta_comas > 0){
            query += ',';
            cuenta_comas = 0;
        }
        query+= ` Color = '${color}'`;
        cuenta_comas++;
    }

    if(modelo){
        if(cuenta_comas > 0){
            query += ',';
            cuenta_comas = 0;
        }
        query+= ` Modelo = '${modelo}'`;
        cuenta_comas++;
    }

    if(posicion){
        if(cuenta_comas > 0){
            query += ',';
            cuenta_comas = 0;
        }
        query+= ` Posicion = '${posicion}'`;
        cuenta_comas++;
    }

    query += ` WHERE Placas = '${placas}'`;

    
    config.pool.getConnection(function(err, conn) {
        
        conn.query(
            query,
            function(err, results, fields) {
                if(err){
                    console.log(err);
                }else{
                    return res.status(200).send({ message: `el vehiculo con placas :  ${placas} se actualizo exitosamente` });
                }
            }
          );
        config.pool.releaseConnection(conn);
     })
}

function delete_Vehiculos(req,res){

    var placas = req.body.placas;

    config.pool.getConnection(function(err, conn) {
        
        conn.execute(
            'DELETE FROM vehiculos WHERE Placas = ?',
            [placas],
            function(err, results, fields) {
                if(!err){
                    return res.status(200).send({ message: `El vehiculo con las placas: ${placas} fue eliminado exitosamente` });
                }else{
                    console.log(err);
                }               
            }
          );

        config.pool.releaseConnection(conn);
     })
}

module.exports = {
    agregar_Vehiculo,
    get_Vehiculos,
    update_Vehiculos,
    delete_Vehiculos
}