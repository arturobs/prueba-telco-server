'use strict'

function prueba(req,res){
    res.status(200).send({ message: 'exito de prueba de la API TELCO!'});
}

function prueba_Privada(req,res){
    res.status(200).send({ message: 'exito de prueba de token de la API TELCO!'});
}

module.exports = {
    prueba,
    prueba_Privada
}